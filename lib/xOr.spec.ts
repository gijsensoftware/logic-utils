import { xOr } from './xOr';

describe('xOr', () => {
    it('should perform an exclusive or', () => {
        expect(xOr(false, false)).toBeFalsy();
        expect(xOr(true, false)).toBeTruthy();
        expect(xOr(false, true)).toBeTruthy();
        expect(xOr(true, true)).toBeFalsy();
    });
});
