export const xOr = (a: boolean, b: boolean) => {
    return (a || b) && !(a && b);
}
